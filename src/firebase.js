import firebase from 'firebase'
const config = {
  apiKey: "AIzaSyAwPWpOktIIBFtf8u8RfW7WbSGKoHbt3rQ",
  authDomain: "polling-app-aecfd.firebaseapp.com",
  databaseURL: "https://polling-app-aecfd.firebaseio.com",
  projectId: "polling-app-aecfd",
  storageBucket: "polling-app-aecfd.appspot.com",
  messagingSenderId: "225933586226",
  appId: "1:225933586226:web:e108d8e4d219b313"
  };
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;