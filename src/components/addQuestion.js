import React, { Component } from 'react';
import firebase, { } from '../firebase.js';
import { Link } from 'react-router-dom';

class AddQuestion extends Component {

  constructor(props) {
    super(props);
    this.projectRef = firebase.firestore().collection('project');
    this.state = {
        Title: 'title',
        mainCategory: '',
        secondaryLevelboo: false,
        secondaryCategory: '',
        subtitle: '',
        username: '',
        items: [],
        resources: [{name: '',bucketType: '', description: '', assets: [{title: '', link: '', type: ''}]}]
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  
  }
  handleChangeCheckbox(e) {
    this.setState({
        [e.target.name]: e.target.checked
    });
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  


  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.mainCategory) {
        alert ('Please enter a title');
    } else {
            this.projectRef.add({
                mainCategory: this.state.mainCategory,
                secondaryLevelboo: this.state.secondaryLevelboo,
                secondaryCategory: this.state.secondaryCategory,
                resources: this.state.resources
            });  
            this.setState({
              mainCategory: '',
              secondaryLevelboo: false,
              secondaryCategory: '',
              subtitle: '',
              username: '',
              items: [],
              resources: [{name: '',bucketType: '', description: '', assets: [{title: '', link: '', type: ''}]}]
            });
    }
  }

  removeItem(itemId) {
    this.projectRef.doc(itemId).delete();
  }

  appendInput() {
    var newInput = {name: '',bucketType: '', assets: [{title: '', link: '', type: ''}]}
    this.setState(prevState => ({ resources: prevState.resources.concat([newInput]) }));
  }

  appendInputR(index, indexr) {
     var newInput = {title: '', link: '', type: ''}
     var concat = this.state.resources[index].assets.concat(newInput);
     this.setState(prevState => ({ assets: prevState.resources[index].assets = concat }));
  }

  render() {
    return (
        <div>
            <h2>Add question </h2>
           
        
             <form onSubmit={this.handleSubmit}>
              
             <input type="text" name='Title' placeholder="Titl" onChange={(e) => this.handleChange(e)} value={this.state.Title} />                   
                                

                <button>Submit</button>
            </form>
           
        </div>
    );
  }
}
export default AddQuestion;