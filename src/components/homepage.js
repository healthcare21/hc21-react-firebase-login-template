import React, { Component } from 'react';
import './css/homepage.css';

var _ = require('lodash');

class Details extends Component {

  constructor(props) {
  super(props);
   this.state = {
      currentItem: '',
      username: '',
      items: [],
      user: null,
      currentQuestion: ''
    }
  
  }


  componentDidMount() {
    let userdetails =  localStorage.getItem('username');
  }

  getQuestion(questionId) {
    let newarr;
    if(this.props.currentState !== 'notStarter') {
        //filterquestions by id
      newarr =   _.filter(this.props.currentQuestions, { 'id': this.props.currentState });
     
    }
    return newarr;
  }

  createList = (questions) => {
    let returnQuestion = [];
    if(questions.questions){
    console.log(questions.questions.length);
      for (var i = 0; i < questions.questions.length; i++) {
        returnQuestion.push(<li>{questions.questions[i]}</li>);
      }
    }
    return returnQuestion;
  }

  render() {
    let title; let description; let questions;
    if(!this.props.currentState || this.props.currentState === 'notStarted'){
      //display welcome message - not started!
      title = "The poll has not been started yet, please wait - this screen will automatically refresh when the poll is open";
      description = "Instructions for the poll"
    } else {
      //get question details from props
      let currentQ = this.getQuestion();
      title = currentQ.data;
      if(currentQ[0]) {
        console.log(currentQ[0].data.Descriptions);
        title = currentQ[0].data.Name;
        description = currentQ[0].data.Descriptions;
        questions = currentQ[0].data.questionsArray;
      }
    }
   
    return (
            <div className="homepage">
              HOMEPAGE 
               <h1>{title}</h1>
               <h2>{description}</h2>
                <ul>
                {this.createList({questions})}

                </ul>

            </div>
    )
  }
}
export default Details;