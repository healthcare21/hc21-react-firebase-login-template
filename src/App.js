import React, { Component } from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import './App.css';
import './css/hamburgers.css';
import firebase, { auth } from './firebase.js';
import Homepage from './components/homepage'
import AddQuestion from './components/addQuestion'
import { slide as Menu } from 'react-burger-menu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faLock } from '@fortawesome/free-solid-svg-icons'



class App extends Component {
  constructor() {
    super();

    this.questionRef = firebase.firestore().collection('questions');
    this.userRef = firebase.firestore().collection('users');
    this.currentState = firebase.firestore().collection('currentState');

    this.state = {
      currentItem: '',
      currentQuestions: '',
      currentState: '',
      username: '',
      items: [],
      userlevel: '',
      catagories: [],
      user: null,
      userValues: [],
      header: 'POLL TRACKER',
      secondary: 'HC21 - Event polling'
    }
    this.handleChange = this.handleChange.bind(this);
    this.setheaderText = this.setheaderText.bind(this);
    // this.handleSubmit = this.handleSubmit.bind(this);
    this.login = this.login.bind(this); 
    this.logout = this.logout.bind(this);
    this.unsubscribeCol = this.currentState.onSnapshot(this.populateCurrent);
    this.unsubscribeQuestion = this.questionRef.onSnapshot(this.populateQuestion);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  logout() {
    auth.signOut()
      .then(() => {
        this.setState({
          user: null
        });
      });
  }

  populateUserFire = (snapshot) => {
    const docs = snapshot.docs.map((docSnapshot) => ({
      id: docSnapshot.id,
      data: docSnapshot.data(),
    }));
    localStorage.setItem('username', docs[0].data.name);
    localStorage.setItem('useremail', docs[0].data.email);
  };
  populateCurrent= (snapshot) => {
    this.setState({
      currentState: snapshot.docs[0].data().state
    });
  }

  login(e, username, password) {
    e.preventDefault();
    auth.signInWithEmailAndPassword(this.state.username,this.state.password) 
      .then((result) => {
        const user = result.user;
        //Get user details where email = submitted email
      }) .catch(err => {
         alert(err.message);
      });
  }

  populateState = (snapshot) => {
    const docs = snapshot.docs.map((docSnapshot) => ({
      id: docSnapshot.id,
      data: docSnapshot.data()
    }));

    this.setState({
      items: docs
    });
  };

  populateQuestion= (snapshot) => {
    const docs = snapshot.docs.map((docSnapshot) => ({
      id: docSnapshot.id,
      data: docSnapshot.data()
    }));
    this.setState({
      currentQuestions: docs
    });
  };


  componentDidMount() {
   
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
        this.userRef.where("email", "==", this.state.username).onSnapshot(this.populateUserFire);
        
        //Get all questions to pass to props.
        if(user.email.includes("healthcare21")){
          this.setState({
              userlevel: 'admin'
            })
        }
      } 
    });
  }

  //Set header text
  setheaderText(propsTitle) {
    this.setState({
      header: propsTitle,
      secondary: ''
    })
  }


  render() {
    let link;
    if (this.state.userlevel === 'admin'){
      link = <><ul class="menu"><li><Link to="/">Home</Link></li><li><Link to="/myKameo">Questions </Link></li></ul></>
    }
    return (
      <div className='app'>
        <header>
            <div className="wrapper">
            <h1>{this.state.header}</h1>
            <h3>{this.state.secondary}</h3>
                {this.state.user ?
                  <p className="headerIcon" onClick={this.logout}><FontAwesomeIcon icon={faLock} /></p>                
                  :
                  <div></div>
                }
            </div>
        </header>
        {this.state.user ?
          <BrowserRouter >
              <Menu right>
                {link}
              </Menu>
         
            <Route path="/" exact render={()=> <Homepage currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState}  />} />
            <Route path="/addNew:id" exact render={()=> <AddQuestion currentQuestions={this.state.currentQuestions} userValues={this.state.userValues} currentState={this.state.currentState}  />} />
           
          </BrowserRouter>
            :
          <div className='wrapper'>
            <h2>Welcome to HC21 event Macclesfield.</h2>
            {this.state.user ?
                        <button onClick={this.logout}>Log Out</button>                
                        :
                        <form onSubmit={this.login} className="loginForm">
                        <h4>Please log in</h4>
                        <input type="text" name="username" placeholder="Username" onChange={this.handleChange}/>
                        <input type="password" name="password" placeholder="Password?" onChange={this.handleChange}/>
                        <button>Login</button>
                      </form>
                      }
          </div>
      }
      </div>
    );
  }
}
export default App;